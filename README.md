# Recipe Manager

Our application is a comprehensive and user-friendly application designed to simplify the way users interact with culinary recipes. It offers a seamless experience for managing a vast collection of recipes, allowing users to effortlessly search, modify, and organize them. Key features include advanced filtering options by name, ingredients, type (like desserts or main courses), and preparation time. The application stands out with its ability to let users modify existing recipes or add new ones to their collection. Moreover, it provides a unique and practical feature where users can generate customized shopping lists based on the recipes they select. This makes meal planning and grocery shopping more efficient and tailored to the users' needs. Whether you are a cooking enthusiast or a professional chef, this Scala-based application is designed to enhance your culinary experience by bringing a digital twist to traditional recipe management.


## Key feature : 

 -   Search Recipes by Ingredient: Users can search for recipes that include specific ingredients.

-   Search Recipes by Dish Type: This feature allows users to find recipes based on their dish type, such as desserts or breakfast.

-   Search Recipes by Preparation Time: Users can search for recipes based on the time required for preparation.

-   Search Recipes by Difficulty: This function enables users to filter recipes according to the level of cooking difficulty.

-   Generate Shopping List: The application can generate a shopping list from a selected recipe, detailing the necessary ingredients.

-   Update Recipe Ingredients: Users have the capability to update the ingredients of an existing recipe.

These functionalities provide a comprehensive and versatile tool for recipe management, catering to a variety of user needs and preferences in cooking.


## Feature Deep Dive : Adding a Recipe

A standout feature of our is the ability to add new recipes. This function is at the heart of the application's interactivity and user engagement. Let's delve into how this feature works:


            // Function to update ingredients in the CSV file
            def updateIngredients(idToUpdate: Int, newIngredients: String): ZIO[Any, Throwable, Unit] = {
            for {
                // Read the CSV file
                rows <- ZIO.attempt { ... }

                // Modify only the ingredients of the specified line
                updatedRows = rows.map { row =>
                if (row("id").toInt == idToUpdate) {
                    row.updated("ingredients", newIngredients)
                } else row
                }

                // Write the modified data back to the CSV file
                _ <- ZIO.attempt { ... }
            } yield ()
            }


## Exploring the Functionality :

    Method Function: addRecipe is designed to take a Recipe object as input.
    Incorporating New Data: The method integrates the new recipe into the existing collection, updating the in-memory data structure.
    Persistence: After adding the recipe to the collection, the method also updates the CSV file to ensure the new recipe is saved and accessible in future sessions.
    User Interaction: This function is typically invoked through the user interface, where users can input details of a new recipe, such as name, ingredients, and preparation steps.


By highlighting this feature, we showcase the application's dynamic nature, allowing users to continually expand their culinary repertoire.


## Functional Schema

![image](functional_shema.png)


## Getting Started with Scala Recipe Manager

Embark on your culinary journey with the Scala Recipe Manager! Here's a fresh take on getting everything set up:

### Before You Begin

-   Ensure Java 8 or a newer version is up and running on your machine.
-   Scala 3.3.1 should be ready to go on your system.
-   sbt (Scala Build Tool) is a must-have for the magic to happen.

### Setting Up the Kitchen

Fetch the project to your local machine like fetching ingredients for a new dish.

#### Bring the Recipe Manager Home :
    
            git clone [REPOSITORY_LINK]


Enter the project's realm like stepping into your cooking space.

#### Step into the Kitchen :
            cd RecipeManager-Project


Let's get the environment ready, like preheating your oven before baking.

#### Preheat the Code Oven :

            sbt compile



### Let the Cooking Begin

#### Fire Up the Application :
            sbt run


Run the tests like tasting your dish for the perfect flavor balance.

#### Taste Test with Tests :

            sbt test




Good ! You're all set to explore and enjoy the Scala Recipe Manager. Happy cooking! 


This project, which we are proud of, is the result of the collaborative efforts of a dedicated team of three developers:

-   Oussama Daghoui - oussama.daghoui@efrei.net
-   Monde Alex - alex.monde@efrei.net
-   Diop Talla - talla.diop@efrei.net

Each member brought unique skills and perspectives to the project, contributing to its success. If you experience any issues or have any questions regarding our app, we encourage you to contact us. We are committed to providing support and ensuring the best possible experience with our app.
