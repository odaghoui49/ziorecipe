import org.scalatest.funsuite.AnyFunSuite
import domain.Recipe

class RecipeTest extends AnyFunSuite {
  test("Recipe creation with valid parameters") {
    val recipe = Recipe(1, "Spaghetti Bolognese", "Tomatoes, Beef, Spaghetti", 30, "Intermediate", "Main Course")
    assert(recipe.id === 1)
    assert(recipe.name === "Spaghetti Bolognese")
  }

  test("Recipe creation with null name should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, null, "Tomatoes, Beef, Spaghetti", 30, "Intermediate", "Main Course")
    }
  }

  test("Recipe creation with null ingredients should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, "Spaghetti Bolognese", null, 30, "Intermediate", "Main Course")
    }
  }

  test("Recipe creation with null difficulty should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, "Spaghetti Bolognese", "Tomatoes, Beef, Spaghetti", 30, null, "Main Course")
    }
  }

  test("Recipe creation with null dishType should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, "Spaghetti Bolognese", "Tomatoes, Beef, Spaghetti", 30, "Intermediate", null)
    }
  }
  test("Recipe creation with invalid prepTime should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, "Spaghetti Bolognese", "Tomatoes, Beef, Spaghetti", 3, "Intermediate", "Main Course")
    }
  }

  test("Recipe creation with invalid difficulty should fail") {
    assertThrows[IllegalArgumentException] {
      Recipe(1, "Spaghetti Bolognese", "Tomatoes, Beef, Spaghetti", 30, "Hard", "Main Course")
    }
  }

}
