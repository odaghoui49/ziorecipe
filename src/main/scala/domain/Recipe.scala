package domain

final case class Recipe(id: Int, name: String, ingredients: String, prepTime: Int, difficulty: String, dishType: String) {
  require(name != null, "Name cannot be null")
  require(ingredients != null, "Ingredients cannot be null")
  require(prepTime >= 5, "Preparation time must be at least 5 minutes")
  require(Seq("Easy", "Intermediate").contains(difficulty), "Difficulty must be either 'Easy' or 'Intermediate'")
  require(dishType != null, "Dish Type cannot be null")
}
