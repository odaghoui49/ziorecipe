import java.io.IOException
import CsvReader._
import repository.CsvWriter._
import domain._
import RecipeSearchFunctions._
import zio._
import zio.Console._
import zio.Duration._

// Define a common function to display recipes
def displayRecipes(results: List[Recipe]): ZIO[Any, IOException, List[Unit]] = {
  Console.printLine("\n--------------------------------------------------------")
  ZIO.foreach(results) { recipe =>
    Console.printLine(s"${recipe.id}: ${recipe.name} - ingredients: ${recipe.ingredients} - Preparation time: ${recipe.prepTime} minutes")
  }
}

object Main extends ZIOAppDefault {

  val Main: ZIO[ZIOAppArgs & Scope, Throwable, Unit] = for {
    _ <- Console.printLine("\n--------------------------------------------------------")
    _ <- Console.printLine("Choose your search type:")
    _ <- Console.printLine("[1] Search recipes by ingredient")
    _ <- Console.printLine("[2] Search recipes by dish type")
    _ <- Console.printLine("[3] Search recipes by preparation time")
    _ <- Console.printLine("[4] Search recipes by difficulty")
    _ <- Console.printLine("[5] Generate a shopping list based on a selected recipe")
    _ <- Console.printLine("[6] Update recipe ingredients")
    _ <- Console.printLine("[7] Add a new recipe") // New option for adding a recipe
    _ <- Console.printLine("\n--------------------------------------------------------")

    // Loop until a valid choice is made
    _ <- readLine("Enter the number corresponding to your choice (1/2/3/4/5/6/7):").flatMap { choice =>
      loadRecipes().flatMap { recipes =>
        choice match {
          case "1" =>
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              ingredient <- readLine("Enter the ingredient you want to use (e.g., chicken, tomatoes): ")
              results    <- searchByIngredient(ingredient, recipes)
              _          <- displayRecipes(results)
            } yield ()

          case "2" =>
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              dishType   <- readLine("Enter the dish type you are looking for (e.g., Dessert, Breakfast):")
              results    <- searchByType(dishType, recipes)
              _          <- displayRecipes(results)
            } yield ()

          case "3" =>
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              duration <- readLine("Enter the desired preparation time in minutes (e.g., 30, 45):")
              results <- searchByDuration(duration.toInt, recipes)
              _          <- displayRecipes(results)
            } yield ()

          case "4" =>
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              difficulty <- readLine("Enter the desired difficulty level (e.g., Easy, Intermediate):")
              results    <- searchByDifficulty(difficulty, recipes)
              _          <- displayRecipes(results)
            } yield ()

          case "5" =>
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              _ <- Console.printLine("The list of recipes will be displayed. Please choose your recipe.")
              _ <- Console.printLine("--------------------------------------------------------\n")
              _ <- ZIO.sleep(3.seconds)
              _          <- ZIO.foreach(recipes) { recipe =>
                Console.printLine(s"${recipe.id}: ${recipe.name} ")
              }
              _ <- Console.printLine("\n--------------------------------------------------------")
              id <- readLine("Enter the ID of the recipe to generate a shopping list:(e.g., 5, 40): ")
              results    <- searchByName(id.toInt, recipes)
              _          <- ZIO.foreach(results) { results =>
                Console.printLine("Shopping list:")
                Console.printLine(s"- Ingredients: ${results.ingredients}")
              }
            } yield ()

          case "6" =>
            for {
              _ <- Console.printLine("The list of recipes will be displayed. Please choose your recipe.")
              _ <- Console.printLine("--------------------------------------------------------\n")
              _ <- ZIO.sleep(3.seconds)
              _ <- ZIO.foreach(recipes) { recipe =>
                Console.printLine(s"${recipe.id}: ${recipe.name} ")
              }
              _ <- Console.printLine("\n--------------------------------------------------------")
              id <- readLine("Enter the ID of the recipe you want to update: ").map(_.toInt)
              newIngredients <- readLine("Enter the new ingredients: ")
              updatedRecipes <- updateIngredients(id, newIngredients) // You need to implement this function
              _ <- Console.printLine("The ingredient has undergone a significant change.")
            } yield ()

          case "7" => // New case for adding a recipe
            for {
              _ <- Console.printLine("\n--------------------------------------------------------")
              recipes <- loadRecipes()
              // Generate ID for the new recipe as the last ID plus one
              id = if (recipes.isEmpty) 1 else recipes.map(_.id).max + 1
              name <- readLine("Enter the name for the new recipe: ")
              ingredients <- readLine("Enter the ingredients for the new recipe: ")
              prepTime <- readLine("Enter the preparation time for the new recipe (in minutes): ").map(_.toInt)
              difficulty <- readLine("Enter the difficulty for the new recipe (e.g., Easy, Intermediate) : ")
              dishType <- readLine("Enter the dish type for the new recipe (e.g., Dessert, Breakfast): ")

              newRecipe = Recipe(id, name, ingredients, prepTime, difficulty, dishType)
              _ <- addRecipe(newRecipe) // Add the new recipe
              _ <- Console.printLine("The new recipe has been added.")
            } yield ()

          case _ =>
            ZIO.fail(new Exception("Invalid choice. Please choose either 1/2/3/4/5/6/7."))
        }
      }
    }
  } yield ()

  val run = Main
}
