import java.io.IOException
import domain._
import RecipeSearchFunctions._
import zio._
import zio.Console._
import zio.Duration._


object RecipeSearchFunctions {

  // Function to search recipes by ingredient
  def searchByIngredient(ingredient: String, recipes: List[Recipe]): UIO[List[Recipe]] =
    ZIO.succeed(recipes.filter(_.ingredients.toLowerCase.contains(ingredient.toLowerCase)))

  // Function to search recipes by dish type
  def searchByType(dishType: String, recipes: List[Recipe]): UIO[List[Recipe]] =
    ZIO.succeed(recipes.filter(_.dishType.toLowerCase.contains(dishType.toLowerCase)))

  // Function to search recipes by maximum preparation time
  def searchByDuration(maxPrepTime: Int, recipes: List[Recipe]): UIO[List[Recipe]] =
    ZIO.succeed(recipes.filter(_.prepTime <= maxPrepTime))

  // Function to search recipes by difficulty level
  def searchByDifficulty(difficulty: String, recipes: List[Recipe]): UIO[List[Recipe]] =
    ZIO.succeed(recipes.filter(_.difficulty == difficulty))

  // Function to search recipes by ID
  def searchByName(id: Int, recipes: List[Recipe]): UIO[List[Recipe]] =
    ZIO.succeed(recipes.filter(_.id == id))

}