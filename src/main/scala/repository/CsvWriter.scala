package repository

import zio.ZIO
import com.github.tototoshi.csv._
import java.io.File
import domain._

object CsvWriter {

  // Path to the CSV file
  val filePath = "src/main/resources/recipes.csv"

  // Function to update ingredients in the CSV file
  def updateIngredients(idToUpdate: Int, newIngredients: String): ZIO[Any, Throwable, Unit] = {
    for {
      // Read the CSV file
      rows <- ZIO.attempt {
        val reader = CSVReader.open(new File(filePath))
        val allRows = reader.allWithHeaders()
        reader.close()
        allRows
      }

      // Modify only the ingredients of the specified line
      updatedRows = rows.map { row =>
        if (row("id").toInt == idToUpdate) {
          row.updated("ingredients", newIngredients)
        } else row
      }

      // Write the modified data back to the CSV file
      _ <- ZIO.attempt {
        val writer = CSVWriter.open(new File(filePath))
        val headers = List("id", "name", "ingredients", "prepTime", "difficulty", "dishType")
        writer.writeRow(headers)
        updatedRows.foreach(row => writer.writeRow(headers.map(row)))
        writer.close()
      }
    } yield ()
  }

  def addRecipe(recipe: Recipe): ZIO[Any, Throwable, Unit] = {
    for {
      // Read the CSV file
      rows <- ZIO.attempt {
        val reader = CSVReader.open(new File(filePath))
        val allRows = reader.allWithHeaders()
        reader.close()
        allRows
      }

      // Add the new recipe to the existing rows
      updatedRows = rows :+ Map(
        "id" -> recipe.id.toString,
        "name" -> recipe.name,
        "ingredients" -> recipe.ingredients,
        "prepTime" -> recipe.prepTime.toString,
        "difficulty" -> recipe.difficulty,
        "dishType" -> recipe.dishType
      )

      // Write the updated data to the CSV file
      _ <- ZIO.attempt {
        val writer = CSVWriter.open(new File(filePath))
        val headers = List("id", "name", "ingredients", "prepTime", "difficulty", "dishType")
        writer.writeRow(headers)
        updatedRows.foreach(row => writer.writeRow(headers.map(row)))
        writer.close()
      }
    } yield ()
  }
}