import java.io.File
import java.time.LocalDate
import com.github.tototoshi.csv._
import zio._
import zio.stream.ZStream
import domain._
import scala.io.Source


object CsvReader {

  // Function to load recipes from a CSV file
  def loadRecipes(): ZIO[Any & ZIOAppArgs & Scope, Throwable, List[Recipe]] =
    for {
      // Open the CSV file as a resource
      source <- ZIO.succeed(CSVReader.open(Source.fromResource("recipes.csv")))

      // Using ZStream to process the CSV file as a stream of lines
      recipes <- ZStream
        .fromIterator[Seq[String]](source.iterator)
        .drop(1) // Skipping the header row
        .map[Option[Recipe]](line =>
          Some(
            Recipe(
              id = line(0).toInt,
              name = line(1),
              ingredients = line(2),
              prepTime = line(3).toInt,
              difficulty = line(4),
              dishType = line(5)
            )
          )
        )
        .collectSome[Recipe]
        .runCollect

      // Close the CSV file source
      _ <- ZIO.succeed(source.close())

      // Convert the recipes stream into a List
      recipesList <- ZIO.succeed(recipes.toList)
    } yield recipesList
}